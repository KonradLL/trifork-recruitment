package model;

import nl.trifork.tictactoe.exception.TimerNotStartedException;

public class Timer {
    private long startTime;
    private long endTime;

    public Timer() {
    }

    public void start() {
        this.startTime = System.nanoTime();
    }

    public void stop() {
        this.endTime = System.nanoTime();
    }

    public int getTime() throws TimerNotStartedException {
        if (this.startTime == 0) {
            throw new TimerNotStartedException();
        }

        if (this.endTime != 0) {
            return (int)(this.endTime - this.startTime);
        } else {
            return (int)(System.nanoTime() - this.startTime);
        }
    }
}

package model;


import nl.trifork.tictactoe.exception.TimerNotStartedException;

public class Score {
    private final int MAX_SCORE = 100000;
    private short moves;
    private Timer timer = new Timer();
    private Player player;

    public Score(Player player) {
        this.player = player;
        this.timer.start();
    }

    //necessary for front-end
    public Player getPlayer() {
        return player;
    }

    public int getValue() {
        try {
            long timeScore = timer.getTime() / (long) 100000;
            int scorePenaly = moves * (int) timeScore;
            int score = MAX_SCORE - scorePenaly;
            return (score > 0) ? score : 0;
        } catch (TimerNotStartedException err) { //TODO a cleaner exception handle
            System.out.println("score timer not started, should be started in constructor"); //logging
            return 0;
        }
    }



    public void addMove() {
        this.moves++;
    }

    public void finish() {
        this.timer.stop();
    }
}

package model;

public class Player {
    private String name;
    private boolean piece; //true is 'o' false is 'x'

    public Player(String name, boolean pieceType) {
        this.name = name;
        this.piece = pieceType;
    }

    public char getPiece() {
        if (this.piece) {
            return 'o';
        } else {
            return 'x';
        }
    }

    public String getName() {
        return name;
    }
}

package model;

import nl.trifork.tictactoe.exception.BoardSetException;
import nl.trifork.tictactoe.exception.GameFinishedException;

public class Match {

    private final short BOARD_LENGTH = 3;
    private Board board = new Board();
    private Player player;
    private Player computer;
    private MatchStatus matchStatus = MatchStatus.UNDECIDED;
    private Score score;

    public Match(Player player, Player computer) {
        this.player = player;
        this.computer = computer;
        this.score = new Score(player);
    }

    public MatchStatus getMatchStatus() {
        return this.matchStatus;
    }

    public Score getScore() {
        return this.score;
    }

    public Board getBoard() {
        return this.board;
    }

    public void setPlayerMoveAndCheckWin(short column, short row) throws BoardSetException, GameFinishedException {
        setMoveAndCheckWin(this.player.getPiece(), column, row);
        this.score.addMove();
    }

    public void setComputerMoveAndCheckWin(short column, short row) throws BoardSetException, GameFinishedException {
        setMoveAndCheckWin(this.computer.getPiece(), column, row);
    }

    private void setMoveAndCheckWin(char piece, short column, short row) throws BoardSetException, GameFinishedException {
        checkMoveValidity(column, row);
        this.board.setBoardSpace(piece, column, row);
        checkWinner(piece, column, row);
    }

    private void checkMoveValidity(short column, short row) throws BoardSetException, GameFinishedException {
        if (this.matchStatus != MatchStatus.UNDECIDED) {
            throw new GameFinishedException("Can't set on a match that is already decided");
        } else if (!this.board.isEmptySpace(column, row)) {
            throw new BoardSetException(String.format("board piece column '%d' row '%d' is already set", column, row));
        }
    }

    private void checkWinner(char piece, short row, short column) {
        boolean hasWon = board.pieceWon(piece, row, column);

        if (hasWon && player.getPiece() == piece) {
            this.score.finish();
            this.matchStatus = (player.getPiece() == piece) ? MatchStatus.PLAYER_WON : MatchStatus.COMPUTER_WON;
        } else if (board.isDraw()) {
            this.score.finish();
            this.matchStatus = MatchStatus.DRAW;
        }
    }
}

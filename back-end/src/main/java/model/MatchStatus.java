package model;

public enum MatchStatus {
    UNDECIDED,
    PLAYER_WON,
    COMPUTER_WON,
    DRAW
}

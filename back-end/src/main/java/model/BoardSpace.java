package model;

public class BoardSpace {

    private short column;
    private short row;

    public BoardSpace(short column, short row) {
        this.column = column;
        this.row = row;
    }

    public short getColumn() {
        return column;
    }

    public short getRow() {
        return row;
    }
}

package model;

import java.util.ArrayList;
import java.util.List;

public class Board {

    private final short BOARD_LENGTH = 3;
    private char[][] board = new char[3][3];

    public Board() {
    }

    public char[][] getBoard() {
        return board;
    }

    public char getBoardSpace(short column, short row) {
        return board[column][row];
    }

    public void setBoardSpace(char piece, short row, short column) {
        this.board[column][row] = piece;
    }

    public List<BoardSpace> getEmptySpaces() {
        List<BoardSpace> emptySpaces = new ArrayList<>();
        for (short column = 0; column < 3; column++) {
            emptySpaces.addAll(getEmptyColumnSpaces(column));
        }
        return emptySpaces;
    }

    private ArrayList<BoardSpace> getEmptyColumnSpaces(short column) {
        ArrayList<BoardSpace> emptySpaces = new ArrayList<>();
        for (short row = 0; row < 3; row++) {
            if (isEmptySpace(column, row)) {
                BoardSpace boardSpace = new BoardSpace(column, row);
                emptySpaces.add(boardSpace);
            }
        }
        return emptySpaces;
    }

    public boolean isEmptySpace(short column, short row) {
        return (this.board[column][row] == '\u0000') ? true : false;
    }

    public boolean isDraw() {
        short filledSpaces = 0;
        for (short column = 0; column < 3; column++) {
            for (short row = 0; row < 3; row++) {
                if (!isEmptySpace(column, row)) {
                    filledSpaces++;
                }
            }
        }
        return (filledSpaces == 9) ? true : false;
    }

    public boolean pieceWon(char piece, short column, short row) {
        if (pieceWonHorizontal(piece, column)) { //check horizontal
            return true;
        }

        if (pieceWonVertical(piece, row)) { //check vertical
            return true;
        }

        boolean isOnDiagonal = row == column;
        if (isOnDiagonal && pieceWonDiagonal(piece)) { //check diagonal if piece is on diagonal
            return true;
        }

        boolean isOnAntiDiagonal = row + column == BOARD_LENGTH - 1;
        if (isOnAntiDiagonal && pieceWonAntiDiagonal(piece)) { //check diagonal if piece is on anti-diagonal
            return true;
        }

        return false;
    }

    private boolean pieceWonHorizontal(char piece, short column) {
        for(int row = 0; row < 3; row++){
            if(board[column][row] != piece) {
                return false;
            }
        }
        return true;
    }

    private boolean pieceWonVertical(char piece, short row) {
        for(int column = 0; column < 3; column++){
            if(board[column][row] != piece) {
                return false;
            }
        }
        return true;
    }

    private boolean pieceWonDiagonal(char piece) {
        for(int i = 0; i < 3; i++){
            if(board[i][i] != piece) {
                return false;
            }
        }
        return true;
    }

    private boolean pieceWonAntiDiagonal(char piece) {
        for(int i = 0; i < 3; i++){
            if(board[i][i] != piece) {
                return false;
            }
        }
        return true;
    }


}

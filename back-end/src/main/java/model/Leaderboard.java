package model;

import java.util.LinkedList;

public class Leaderboard {
    private LinkedList<Score> scores = new LinkedList<>();

    public Leaderboard() {
    }

    public LinkedList<Score> getScores() {
        return this.scores;
    }

    //TODO sorted insert
    public void addScore(Score score) {

//        Score firstScore = scores.get(0);
//        Score lastScore = scores.get(scores.size() - 1);
        if (scores.size() == 0) {
            scores.add(score);
        } else if (scores.get(0).getValue() > score.getValue()) { //compare beginning
            scores.add(0, score);
        } else if (scores.get(scores.size() - 1).getValue() < score.getValue()) { //compare end
            scores.add(scores.size(), score);
        } else {
            int index = 0;
            while (scores.get(index).getValue() < score.getValue()) {
                index++;
            }
            scores.add(index, score);
        }
    }
}
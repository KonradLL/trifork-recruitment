package nl.trifork.tictactoe.service;

import model.*;
import nl.trifork.tictactoe.exception.BoardSetException;
import nl.trifork.tictactoe.exception.GameFinishedException;
import nl.trifork.tictactoe.exception.InvalidMoveException;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Service
public class GameLogicService {

    private Leaderboard leaderboard = new Leaderboard();
    LinkedHashMap<UUID, Match> matchesList = new LinkedHashMap<UUID, Match>();

    public List<Score> getLeaderboardTop(int amount) {
        List<Score> scores = leaderboard.getScores();
        int getAmount = (scores.size() < amount) ? scores.size() : amount;
        return scores.subList(0, getAmount);
    }

    public UUID createMatch(Player player, Player computer) {
        //TODO unit-test check if both have the same piece
        Match newMatch = new Match(player, computer);
        UUID uuid = java.util.UUID.randomUUID();
        matchesList.put(uuid, newMatch);
        return uuid;
    }

    public Match makeMove(UUID matchId, short column, short row) throws InvalidMoveException {
        Match match = matchesList.get(matchId);

        try { // player move
            match.setPlayerMoveAndCheckWin(column, row);
        } catch (BoardSetException error) {
            throw new InvalidMoveException(String.format("Player move for column '%d', row '%d' is already made", column, row));
        } catch (GameFinishedException error) {
            return match;
        }

        if (match.getMatchStatus() == MatchStatus.PLAYER_WON) {
            //TODO add to leaderboard
            leaderboard.addScore(match.getScore());
            return match;
        }

        //if not won, do pc turn
        try { // computer move
            BoardSpace computerMove = getRandomComputerMove(match);
            match.setComputerMoveAndCheckWin(computerMove.getColumn(), computerMove.getRow());
        } catch (BoardSetException error) {
            throw new InvalidMoveException("I screwed up, computer made an invalid move" + error.getMessage());
        } catch (GameFinishedException error) {
            throw new InvalidMoveException("what the hell, this should not happen. comp moved on a finished match");
        }

        return match;
    }

    private BoardSpace getRandomComputerMove(Match match) {
        List<BoardSpace> emptySpaces = match.getBoard().getEmptySpaces();
        Random rand = new Random();
        int randomIndex = rand.nextInt(emptySpaces.size());
        return emptySpaces.get(randomIndex);
    }

}

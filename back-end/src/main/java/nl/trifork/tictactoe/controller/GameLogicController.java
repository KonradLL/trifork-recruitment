package nl.trifork.tictactoe.controller;

import model.Match;
import model.Player;
import model.Score;
import nl.trifork.tictactoe.exception.InvalidMoveException;
import nl.trifork.tictactoe.service.GameLogicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@RestController
public class GameLogicController {

    private final GameLogicService gameLogicService;

    @Autowired
    public GameLogicController(GameLogicService gameLogicService) {
        this.gameLogicService = gameLogicService;
    }

    @CrossOrigin
    @GetMapping("/leaderboard")
    public List<Score> leaderboard(@RequestParam int amount) {
        return gameLogicService.getLeaderboardTop(amount);
    }

    @CrossOrigin
    @PostMapping("/executeTurn")
    public ResponseEntity<?> move(@CookieValue("matchId") UUID matchId, @RequestBody Map<String,Object> body) {
        short column = (short) body.get("column");
        short row = (short) body.get("row");

        if (column > 3 || row > 3) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Column and/or row cannot exceed 3");
        }

        try { // check winner
            Match match = gameLogicService.makeMove(matchId, column, row);
            return ResponseEntity.status(HttpStatus.OK).body(match);
        } catch (InvalidMoveException error) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error.getMessage());
        }
    }

    @CrossOrigin
    @PostMapping(path = "/startMatch", consumes = "application/json", produces = "application/json")
    public ResponseEntity<?> startCompMatch(@RequestBody Map<String,Object> body, HttpServletResponse response) {
        Player player = new Player(body.get("playerName").toString(), (Boolean) body.get("isPieceO"));
        Player computer = new Player("comp", !(Boolean) body.get("isPieceO"));

        UUID matchId = gameLogicService.createMatch(player, computer);
        response.addCookie(new Cookie("matchId", matchId.toString()));
        return ResponseEntity.status(HttpStatus.OK).body(player);
    }

}
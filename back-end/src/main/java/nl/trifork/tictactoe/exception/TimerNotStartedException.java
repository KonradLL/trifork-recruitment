package nl.trifork.tictactoe.exception;

public class TimerNotStartedException extends Exception{

    public TimerNotStartedException() {}

    public TimerNotStartedException(String message) {
        super(message);
    }

    public TimerNotStartedException(Throwable cause) {
        super(cause);
    }

    public TimerNotStartedException(String message, Throwable cause) {
        super(message, cause);
    }
}

package nl.trifork.tictactoe.exception;

import javax.security.auth.login.AccountNotFoundException;

public class BoardSetException extends Exception {

    public BoardSetException() {}

    public BoardSetException(String message) {
        super(message);
    }

    public BoardSetException(Throwable cause) {
        super(cause);
    }

    public BoardSetException(String message, Throwable cause) {
        super(message, cause);
    }

}
